+++
title = 'Home'
publishDate = 2024-04-25T18:14:47-06:00
draft = false
+++

# Upcoming meetings

Meetings will be announced on the mailing list.

> Bring us your tired, your sick, your huddled masses of computers, yearning to breathe free. -- Brian K.

- Wed Apr 3, 2024 - 5PM-8PM - JoCo Central Library
- Wed May 1, 2024 - 6:30PM-8PM - MCPL Colbern Library

## KCLUG on IRC

KCLUG has an IRC channel on the Libera.Chat network. You can access it with any IRC client using the following URL.

[irc://irc.libera.chat:6697/#kclug](irc://irc.libera.chat:6697/#kclug) (This URL may not work in all browsers.)

https://web.libera.chat/

